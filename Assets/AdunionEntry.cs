using UnityEngine;
using System.Collections;

public class AdunionEntry : MonoBehaviour
{	
	void OnGUI ()
	{
		#if UNITY_ANDROID
		GUI.skin.label.fontSize = Screen.height / 50;
		GUI.skin.button.fontSize = Screen.height / 50;
		GUILayout.Label("Interstitial Ad" , GUILayout.Height(Screen.height / 12));
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Show 'gamestart' Ad", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width/2)))
		{
			// Show interstitial ad 'gamestart'
			Adunion4Unity.Instance.showInterstitialAd(Adunion4Unity.IAD_TYPE_GAMESTART);

		}
		
		if (GUILayout.Button("Show 'gamepause' Ad", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width/2)))
		{ 
			// Show interstitial ad 'gamepause'
			Adunion4Unity.Instance.showInterstitialAd(Adunion4Unity.IAD_TYPE_GAMEPAUSE);
		}
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Show game gift", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width/2)))
		{
			// Show interstitial ad 'gamegift'
			Adunion4Unity.Instance.showInterstitialAd(Adunion4Unity.IAD_TYPE_GAMEGIFT);
		}
		if (GUILayout.Button("Show 'gameexit' Ad", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width/2)))
		{
			// stop loading interstitial ad
			Adunion4Unity.Instance.showInterstitialAd(Adunion4Unity.IAD_TYPE_GAMEEXIT);
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Preload Interstitial", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width/2)))
		{
			// stop loading interstitial ad
			Adunion4Unity.Instance.preloadInterstitialAd();
		}
		if (GUILayout.Button("StopLoading", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width/2)))
		{
			// stop loading interstitial ad
			Adunion4Unity.Instance.stopLoadingInterstitialAd();
		}
		GUILayout.EndHorizontal();


		GUILayout.Label("Banner Ad", GUILayout.Height(Screen.height / 12));
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Show Banner Ad", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width/2)))
		{
			// Show banner ad
			Adunion4Unity.Instance.showBannerAd(Adunion4Unity.BAD_POS_BOTTOM_CENTER);
		}
		if (GUILayout.Button("Close Banner Ad", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width/2)))
		{
			// Close banner ad
			Adunion4Unity.Instance.closeBannerAd();
		}
		GUILayout.EndHorizontal();
		if (GUILayout.Button("Preload Banner", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width)))
		{
			// Show banner ad
			Adunion4Unity.Instance.preloadBannerAd();
		}
		
		GUILayout.Label("Link To", GUILayout.Height(Screen.height / 12));
		if (GUILayout.Button("Link to 'moregame'", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width)))
		{
			// Link to more game
			Adunion4Unity.Instance.linkTo(Adunion4Unity.LINK_TYPE_MOREGAME);
		}
		if (GUILayout.Button("Link to 'gamescore'", GUILayout.Height(Screen.height / 12), GUILayout.Width(Screen.width)))
		{
			// Link to google store for game score 
			Adunion4Unity.Instance.linkTo(Adunion4Unity.LINK_TYPE_GAMESCORE);
		}
		
		GUILayout.Label("ltad-sdk-v3.0.6-unity4.3(C#)" , GUILayout.Height(Screen.height / 12));
		#endif
	}
	
	void Update ()
	{
		#if UNITY_ANDROID
		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Home))
		{ 
			// Destroy cache
			Adunion4Unity.Instance.destroy();
			Application.Quit();
		}
		#endif
		
	}
}
