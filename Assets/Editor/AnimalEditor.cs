﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Animal))]
public class AnimalEditor : Editor
{
    void OnSceneGUI()
    {
        if (EditorApplication.currentScene.Contains("LevelEditor"))
        {
            if (Selection.activeTransform != null && Selection.activeTransform.parent != null && Selection.activeTransform.parent.name == "Planet")
            {
                if (LevelEditor.GetPrefabPath(Selection.activeTransform.gameObject).Contains("Animal"))
                {
                    LevelEditor.FixAngle(Selection.activeTransform);
                    LevelEditor.FixPosition(Selection.activeTransform);
                }
            }

            if (Selection.activeTransform != null && Selection.activeTransform.parent != null && Selection.activeTransform.parent.name == "BuildingBlock")
            {
                LevelEditor.FixBuildingBlockPosition();
            }
        }
    }
}
