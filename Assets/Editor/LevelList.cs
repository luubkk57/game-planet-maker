﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Serialization;

[SerializeField]
public class LevelList : EditorWindow
{
    public static LevelList instance;

    public string path = Path.Combine(Application.dataPath, "Resources/Level/LevelManager.xml");
    private Vector2 scroll;

    [SerializeField]
    private List<TextAsset> levels = new List<TextAsset>();

    public static void OpenWindow()
    {
        instance = (LevelList)EditorWindow.GetWindow(typeof(LevelList));
        instance.title = "Level Editor";
    }

    void OnGUI()
    {
        EditorGUILayout.BeginVertical("box");
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Insert New Level"))
        {
            levels.Add(new TextAsset());
        }
        if (GUILayout.Button("Remove All"))
        {
            if (levels.Count > 0)
            {
                for (int i = levels.Count - 1; i >= 0; i--)
                {
                    levels.RemoveAt(i);
                }
            }
        }
        if (GUILayout.Button("Save"))
        {
            Save();
        }
        if (GUILayout.Button("Load"))
        {
            Load();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        GUILayout.Label("Level List:", EditorStyles.boldLabel);
        scroll = GUILayout.BeginScrollView(scroll);
        for (int i = 0; i < levels.Count; i++)
        {
            EditorGUILayout.BeginVertical("box");
            //GUI.backgroundColor = (i % 2 == 0)?Color.cyan:Color.grey;
            levels[i] = (TextAsset)EditorGUILayout.ObjectField(levels[i], typeof(TextAsset), true);
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Level " + (i + 1), EditorStyles.boldLabel);
            if (GUILayout.Button("Insert Above"))
            {
                levels.Insert(i, new TextAsset());
            }
            if (GUILayout.Button("Insert"))
            {
                levels.Insert(i + 1, new TextAsset());
            }
            if (GUILayout.Button("Remove"))
            {
                levels.Remove(levels[i]);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
            //EditorGUILayout.Separator();
        }
        GUILayout.EndScrollView();
    }

    private void Save()
    {
        ClearNull();
        XmlSerializer levelManager = new XmlSerializer(typeof(List<string>));
        FileStream fs = new FileStream(path, FileMode.Create);
        Debug.Log(path);
        List<string> levelList = new List<string>(levels.Count);
        for (int i = 0; i < levels.Count; i++)
        {
            levelList.Add(levels[i].name);
        }
        levelManager.Serialize(fs, levelList);
        fs.Close();
        AssetDatabase.Refresh();
    }
    private void Load()
    {
        TextAsset file = Resources.Load("Level/LevelManager") as TextAsset;
        StringReader filetext = new StringReader(file.text);
        XmlSerializer level = new XmlSerializer(typeof(List<string>));
        List<string> levelList = level.Deserialize(filetext) as List<string>;
        for (int i = levels.Count - 1; i >= 0; i--)
        {
            levels.RemoveAt(i);
        }
        for (int i = 0; i < levelList.Count; i++)
        {
            levels.Add(Resources.Load("Level/" + levelList[i]) as TextAsset);
        }
        filetext.Close();
    }

    private void ClearNull()
    {
        for (int i = levels.Count - 1; i >= 0; i--)
        {
            if (levels[i] == null)
                levels.RemoveAt(i);
        }
    }
}

