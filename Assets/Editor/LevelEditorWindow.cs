﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelEditorWindow : EditorWindow
{
    public static LevelEditorWindow instance;

    public static string levelName = EditorPrefs.GetString("Level_Name");
    public static int difficult = EditorPrefs.GetInt("Level_Difficult");
    public static int rotateSpeed = EditorPrefs.GetInt("Level_Rotation_Speed");

    public GameObject building;

    //public static GameObject planet = GameController.instance.Planet;
    public static Stack<GameObject> buildings;
    public static void OpenWindow()
    {
        instance = (LevelEditorWindow)EditorWindow.GetWindow(typeof(LevelEditorWindow));
        instance.title = "Level Editor";
    }

    void OnGUI()
    {
        GUILayout.Label("Level Info", EditorStyles.boldLabel);
        levelName = EditorGUILayout.TextField("Name", levelName);
        difficult = EditorGUILayout.IntField("Difficult", difficult);
        rotateSpeed = EditorGUILayout.IntField("Rotate Speed", rotateSpeed);

        //if (GUILayout.Button("Remove All Objects"))
        //{
        //    LevelEditor.ClearAll();
        //}
        //if (GUILayout.Button("Arange by Angle"))
        //{
        //    LevelEditor.ArrangeAngle();
        //}
        //if (GUILayout.Button("Arange by Position"))
        //{
        //    LevelEditor.ArrangePosition();
        //}
        //building = (GameObject)EditorGUIUtility.GetObjectPickerObject();
        //if (GUILayout.Button("Pick"))
        //{
        //    EditorGUIUtility.ShowObjectPicker<GameObject>(new UnityEngine.Object(), false, " ", 2);
        //}
        EditorGUILayout.Separator();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Clear"))
        {
            LevelEditor.ClearAll();
        }
        if (GUILayout.Button("Rearrange"))
        {
            LevelEditor.DistributeBuildings();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("<"))
        {
            LevelEditor.LoadLevel(EditorPrefs.GetInt("Current_Level") - 1);
            levelName = EditorPrefs.GetString("Level_Name");
            difficult = EditorPrefs.GetInt("Level_Difficult");
            rotateSpeed = EditorPrefs.GetInt("Level_Rotation_Speed");
            Repaint();
        }
        if (GUILayout.Button(">"))
        {
            LevelEditor.LoadLevel(EditorPrefs.GetInt("Current_Level") + 1);
            levelName = EditorPrefs.GetString("Level_Name");
            difficult = EditorPrefs.GetInt("Level_Difficult");
            rotateSpeed = EditorPrefs.GetInt("Level_Rotation_Speed");
            Repaint();
        }
        if (GUILayout.Button("Save"))
        {
            LevelEditor.Save();
        }
        if (GUILayout.Button("Load"))
        {
            Load();
            Repaint();
        }
        
        EditorGUILayout.EndHorizontal();
        //for (int i = 0; i < LevelEditor.planet.transform.childCount; i++)
        //{
        //    GameObject obj = (GameObject)EditorGUI.ObjectField(new Rect(0, 0, position.width, 10), LevelEditor.planet.transform.GetChild(i), typeof(GameObject));
        //}
    }

    private static void Load()
    {
        LevelEditor.LoadLevel();
        levelName = EditorPrefs.GetString("Level_Name");
        difficult = EditorPrefs.GetInt("Level_Difficult");
        rotateSpeed = EditorPrefs.GetInt("Level_Rotation_Speed");
    }
    private static void Load(int index)
    {
        LevelEditor.LoadLevel(index);
        levelName = EditorPrefs.GetString("Level_Name");
        difficult = EditorPrefs.GetInt("Level_Difficult");
        rotateSpeed = EditorPrefs.GetInt("Level_Rotation_Speed");
    }
}
