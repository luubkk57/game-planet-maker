﻿public class EditorAttributes
{
    public const string LEVEL_NAME = "Level_Name";
    public const string LEVEL_DIFFICULT = "Level_Difficult";
    public const string LEVEL_ROTATION_SPEED = "Level_Rotation_Speed";
    public const string LEVEL_CURRENT = "Current_Level";
}