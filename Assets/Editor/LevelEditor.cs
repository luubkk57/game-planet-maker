﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Serialization;

public class LevelEditor : Editor
{
    public static GameObject planet = GameObject.Find("Planet");
    public static GameObject buildingBlock = GameObject.Find("BuildingBlock");
    //public static GameObject flyingObjects = GameObject.Find("FlyingObjects");
    public static string levelPath = Path.Combine(Path.Combine(Application.dataPath, "Resources"), "Level");
    public static string mapFile;
    public static float defaultFlyingSpeed = 5;

    //public static SerializedProperty sp_planet = serializedObject.FindProperty("Planet");

    //void OnEnable()
    //{
    //    sp_planet = serializedObject.FindProperty("Planet");     
    //}

    [MenuItem("Tools/Level Editor/Level Editor %#L")]
    static void ShowEditorWindow()
    {
        LevelEditorWindow.OpenWindow();
    }
    [MenuItem("Tools/Level Editor/Level List")]
    static void ShowLevelListWindow()
    {
        LevelList.OpenWindow();
    }

    [MenuItem("Tools/Level Editor/Load")]
    public static void LoadLevel()
    {
        ClearAll();
        mapFile = EditorUtility.OpenFilePanel("Open XML File", levelPath, "xml");
        //Debug.Log(mapFile.Length);
        if (mapFile.Length > 0)
        {
            mapFile = mapFile.Substring(mapFile.LastIndexOf("/") + 1);
            LevelEditorWindow.levelName = mapFile.Substring(0, mapFile.LastIndexOf(".xml"));
            EditorPrefs.SetString(EditorAttributes.LEVEL_NAME, mapFile.Substring(0, mapFile.LastIndexOf(".xml")));

            TextAsset levelFile = Resources.Load("Level/" + LevelEditorWindow.levelName) as TextAsset;
            StringReader file = new StringReader(levelFile.text);
            XmlSerializer level = new XmlSerializer(typeof(LevelData));
            LevelData data = level.Deserialize(file) as LevelData;
            file.Close();

            //LevelEditorWindow.levelName = loader.data.name;
            //LevelEditorWindow.rotateSpeed = data.Rotate;
            //LevelEditorWindow.difficult = data.Difficult;
            EditorPrefs.SetInt(EditorAttributes.LEVEL_ROTATION_SPEED, data.Rotate);
            EditorPrefs.SetInt(EditorAttributes.LEVEL_DIFFICULT, data.Difficult);

            ObjectData[] planetObjects = data.PlanetObjects;
            if (!planet) planet = GameObject.Find("Planet");
            for (int i = 0; i < planetObjects.Length; i++)
            {
                Debug.Log(planetObjects[i].name);
                string path = planetObjects[i].type + "/" + planetObjects[i].name;
                GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(Resources.Load(path));
                planet.GetComponent<PlanetController>().PutObject(obj, planetObjects[i].angle, planetObjects[i].height);
                if (planetObjects[i].type == "FloatingObject")
                {
                    obj.GetComponent<FloatingObject>().movingSpeed = planetObjects[i].movingSpeed;
                }
                if (planetObjects[i].type == "Animal")
                {
                    if (planetObjects[i].movingSpeed != 0)
                    {
                        obj.GetComponent<Animal>().movingSpeed = planetObjects[i].movingSpeed;
                    }
                    else
                    {
                        obj.GetComponent<Animal>().movingPattern = planetObjects[i].speedPattern;
                    }
                }
            }

            ObjectData[] buildingBlocks = data.PlayingObjects;
            if (!buildingBlock) buildingBlock = GameObject.Find("BuildingBlock");
            for (int i = 0; i < buildingBlocks.Length; i++)
            {
                string path = buildingBlocks[i].type + "/" + buildingBlocks[i].name;
                GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(Resources.Load(path));
                obj.transform.SetParent(buildingBlock.transform);
            }

            GameObject.Find("SpeedController").GetComponent<SpeedPatternGenerator>().rotationSpeedPattern = data.RotationSpeedPattern;
            FixBuildingBlockPosition();
        }
    }

    [MenuItem("Tools/Level Editor/Open Level Editor")]
    static void Insert()
    {
        Debug.Log(EditorApplication.currentScene);
        if (!EditorApplication.currentScene.Contains("LevelEditor"))
        {
            EditorApplication.OpenScene("LevelEditor");
        }
    }

    [MenuItem("Tools/Level Editor/Save %#A")]
    public static void Save()
    {
        StoreLevelData();
    }

    public static void DistributeBuildings()
    {
        if (!planet) planet = GameObject.Find("Planet");
        int count = planet.transform.childCount;
        int bCount = 0;
        Debug.Log("Distribute " + count);
        for (int i = 0; i < planet.transform.childCount; i++)
        {
            //Debug.Log(GetPrefabPath(planet.transform.GetChild(i).gameObject));
            if (GetPrefabPath(planet.transform.GetChild(i).gameObject).Contains("Building"))
            {
                bCount++;
            }
        }
        //Debug.Log(count);
        for (int i = 0; i < count; i++)
        {
            if (GetPrefabPath(planet.transform.GetChild(i).gameObject).Contains("Building"))
            {
                planet.transform.GetChild(i).localEulerAngles = new Vector3(0, 0, i * 360 / bCount);
                FixPosition(planet.transform.GetChild(i));
                //FixAngle(planet.transform.GetChild(i));
            }
            //Debug.Log(planet.transform.GetChild(i).name + "_" + (i * 360 / bCount));

        }
        //ArrangePosition();
    }

    public delegate void ProcessBuilding(Transform building);

    static void ProcessAllBuilding(ProcessBuilding pb)
    {
        int count = planet.transform.childCount;
        for (int i = 0; i < count; i++)
        {
            pb(planet.transform.GetChild(i));
        }
    }

    //static void ProcessAllFlyingObjects(ProcessBuilding fo)
    //{
    //    int count = flyingObjects.transform.childCount;
    //    for (int i = 0; i < count; i++)
    //    {
    //        fo(flyingObjects.transform.GetChild(i));
    //    }        
    //}

    [MenuItem("Tools/Level Editor/Arrange all by Angle")]
    public static void ArrangeAngle()
    {
        ProcessAllBuilding(FixPosition);
        //ProcessAllFlyingObjects(FixFlyingObjectPosition);
    }

    [MenuItem("Tools/Level Editor/Arrange all by Position")]
    public static void ArrangePosition()
    {
        ProcessAllBuilding(FixAngle);
        ProcessAllBuilding(FixPosition);
        //ProcessAllFlyingObjects(FixFlyingObjectAngle);
    }


    static void StoreLevelData()
    {
        if (planet == null) planet = GameObject.Find("Planet");
        LevelData data = new LevelData();
        data.Difficult = LevelEditorWindow.difficult;
        data.Rotate = LevelEditorWindow.rotateSpeed;
        data.Name = LevelEditorWindow.levelName;
        data.RotationSpeedPattern = GameObject.Find("SpeedController").GetComponent<SpeedPatternGenerator>().rotationSpeedPattern;

        EditorPrefs.SetString(EditorAttributes.LEVEL_NAME, data.Name);
        EditorPrefs.SetInt(EditorAttributes.LEVEL_DIFFICULT, data.Difficult);
        EditorPrefs.SetInt(EditorAttributes.LEVEL_ROTATION_SPEED, data.Rotate);

        data.PlanetObjects = new ObjectData[planet.transform.childCount];
        for (int i = 0; i < planet.transform.childCount; i++)
        {
            data.PlanetObjects[i] = GetObjectData(planet.transform.GetChild(i).gameObject);
        }

        if (buildingBlock == null) buildingBlock = GameObject.Find("BuildingBlock");
        data.PlayingObjects = new ObjectData[buildingBlock.transform.childCount];
        for (int i = 0; i < buildingBlock.transform.childCount; i++)
        {
            data.PlayingObjects[i] = GetObjectData(buildingBlock.transform.GetChild(i).gameObject);
        }
        XmlSerializer level = new XmlSerializer(typeof(LevelData));
        //Debug.Log(levelPath);
        FileStream fs = new FileStream(Path.Combine(levelPath, LevelEditorWindow.levelName + ".xml"), FileMode.Create);
        level.Serialize(fs, data);
        AssetDatabase.Refresh();
        fs.Close();
    }

    static ObjectData GetObjectData(GameObject obj)
    {
        ObjectData objectData = new ObjectData();
        objectData.name = GetPrefabName(obj);
        objectData.type = GetPrefabPath(obj);
        objectData.height = DistanceToPlanet(obj) - GetPlanetRadius();
        objectData.angle = 360 - obj.transform.localEulerAngles.z;
        //Debug.Log(objectData.type);
        if (objectData.type == "FloatingObject")
        {
            objectData.movingSpeed = obj.GetComponent<FloatingObject>().movingSpeed;
        }
        if (objectData.type == "Animal")
        {
            if (obj.GetComponent<Animal>().movingSpeed != 0)
            {
                objectData.movingSpeed = obj.GetComponent<Animal>().movingSpeed;
            }
            else
            {
                if (objectData.speedPattern != null && objectData.speedPattern.Length > 0)
                {
                    objectData.movingSpeed = 0;
                    objectData.speedPattern = obj.GetComponent<Animal>().movingPattern;
                }
                else
                {
                    objectData.movingSpeed = -10;
                }
            }
        }
        //Debug.Log(objectData.name);
        //Debug.Log(objectData.type);
        return objectData;
    }


    [MenuItem("Tools/Level Editor/Clear All %&C")]
    public static void ClearAll()
    {
        //if (!planet) planet = GameObject.Find("Planet");
        //Debug.Log(planet.transform.childCount);
        List<GameObject> objs = new List<GameObject>();
        if (planet)
        {

            for (int i = 0; i < planet.transform.childCount; i++)
            {
                //Debug.Log(planet.transform.GetChild(i).name);
                objs.Add(planet.transform.GetChild(i).gameObject);
                //DestroyImmediate(planet.transform.GetChild(i).gameObject);
            }
        }
        if (buildingBlock)
        {
            for (int i = 0; i < buildingBlock.transform.childCount; i++)
            {
                //Debug.Log(planet.transform.GetChild(i).name);
                objs.Add(buildingBlock.transform.GetChild(i).gameObject);
                //DestroyImmediate(planet.transform.GetChild(i).gameObject);
            }
        }

        foreach (GameObject obj in objs)
        {
            DestroyImmediate(obj);
        }
    }

    //void OnSceneGUI()
    //{
    //    if (Event.current.type == EventType.MouseDown)
    //    {
    //        Debug.Log("Mouse down");
    //    }

    //}

    static void FixFlyingObjectAngle(Transform flyingObject)
    {
        float angle = Vector2.Angle(Vector2.up, flyingObject.position - planet.transform.position)
            * Mathf.Sign(planet.transform.position.x - flyingObject.position.x);
        Vector3 rotation = new Vector3();
        rotation.z = angle;
        flyingObject.transform.eulerAngles = rotation;
    }

    static void FixFlyingObjectPosition(Transform flyingObject)
    {
        //Vector2 pos = new Vector2();
        float radius = Vector2.Distance(flyingObject.position, planet.transform.position);
        Debug.Log(flyingObject.position + "_" + planet.transform.position + "_" + radius);
        float angle = (flyingObject.eulerAngles.z + 90) * Mathf.Deg2Rad;
        Vector3 position = new Vector3();
        position.x = planet.transform.position.x + radius * Mathf.Cos(angle);
        position.y = planet.transform.position.y + radius * Mathf.Sin(angle);
        flyingObject.transform.position = position;
    }

    #region Utils
    static float GetPlanetRadius()
    {
        return planet.GetComponent<CircleCollider2D>().radius;
    }

    public static void FixPosition(Transform building)
    {
        //planet.GetComponent<PlanetController>().SetRadius();
        float radius = GetPlanetRadius();
        if (GetPrefabPath(building.gameObject).Contains("FloatingObject"))
        {
            radius = DistanceToPlanet(building.gameObject);
        }
        float angle = (building.eulerAngles.z + 90) * Mathf.Deg2Rad;
        Vector3 position = new Vector3();
        position.x = planet.transform.position.x + radius * Mathf.Cos(angle);
        position.y = planet.transform.position.y + radius * Mathf.Sin(angle);
        building.transform.position = position;
        //Debug.Log(position);
        //Debug.Log(building.transform.localPosition);
    }

    public static void FixAngle(Transform building)
    {
        //float radius = planet.GetComponent<PlanetController>().radius;
        //Vector2 planetPos = planet.transform.position;
        //Vector2 buildPos = building.position;
        if (!planet) planet = GameObject.Find("Planet");
        float angle = Vector2.Angle(Vector2.up, building.position - planet.transform.position) *
            Mathf.Sign(planet.transform.position.x - building.position.x);
        Vector3 rotation = new Vector3();
        rotation.z = angle;
        building.transform.eulerAngles = rotation;
    }

    static float DistanceToPlanet(GameObject go)
    {
        return Vector2.Distance(go.transform.position, planet.transform.position);
    }

    public static void FixBuildingBlockPosition()
    {
        if (buildingBlock == null) buildingBlock = GameObject.Find("BuildingBlock");
        for (int i = 0; i < buildingBlock.transform.childCount; i++)
        {
            buildingBlock.transform.GetChild(i).localPosition = new Vector2(i * 1.5f, 0.0f);
            GUIStyle style = new GUIStyle();
            style.fontSize = 25;
            style.fontStyle = FontStyle.Bold;
            style.normal.textColor = Color.red;
            style.alignment = TextAnchor.MiddleCenter;
            Handles.Label(buildingBlock.transform.GetChild(i).position, /*"Object_" +*/ i.ToString(), style);
        }
    }
    #endregion

    #region Prefab Utils
    public static string GetPrefabPath(GameObject o)
    {
        return GetPrefabPath(GetPrefabAssetPath(o));
    }
    static string GetPrefabName(string path)
    {
        path = path.Substring(path.LastIndexOf("/") + 1);
        path = path.Substring(0, path.LastIndexOf(".prefab"));
        return path;
    }
    static string GetPrefabName(GameObject o)
    {
        return GetPrefabName(GetPrefabAssetPath(o));
    }
    static string GetPrefabAssetPath(GameObject go)
    {
        return AssetDatabase.GetAssetPath(PrefabUtility.GetPrefabParent(go));
    }
    static string GetPrefabPath(string path)
    {
        if (path.Contains("Resources/"))
        {
            path = path.Substring(path.IndexOf("Resources/") + 10);
            path = path.Substring(0, path.LastIndexOf("/"));
        }
        return path;
    }
    #endregion

    public static void LoadLevel(int index)
    {
        LevelManager.Reload();
        ClearAll();
        //Mathf.Clamp(index, 0, LevelManager.Count - 1);
        if (index < 0) index = 0;
        if (index >= LevelManager.Count) index = LevelManager.Count - 1;
        //Debug.Log(index);
        LevelEditorWindow.levelName = LevelManager.Load(index);
        EditorPrefs.SetInt(EditorAttributes.LEVEL_CURRENT, index);
        EditorPrefs.SetString(EditorAttributes.LEVEL_NAME, LevelManager.Load(index));

        TextAsset levelFile = Resources.Load("Level/" + LevelEditorWindow.levelName) as TextAsset;
        StringReader file = new StringReader(levelFile.text);
        XmlSerializer level = new XmlSerializer(typeof(LevelData));
        LevelData data = level.Deserialize(file) as LevelData;
        file.Close();

        //LevelEditorWindow.levelName = loader.data.name;
        //LevelEditorWindow.rotateSpeed = data.Rotate;
        //LevelEditorWindow.difficult = data.Difficult;
        EditorPrefs.SetInt(EditorAttributes.LEVEL_ROTATION_SPEED, data.Rotate);
        EditorPrefs.SetInt(EditorAttributes.LEVEL_DIFFICULT, data.Difficult);

        ObjectData[] planetObjects = data.PlanetObjects;
        if (!planet) planet = GameObject.Find("Planet");
        for (int i = 0; i < planetObjects.Length; i++)
        {
            Debug.Log(planetObjects[i].name);
            string path = planetObjects[i].type + "/" + planetObjects[i].name;
            GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(Resources.Load(path));
            planet.GetComponent<PlanetController>().PutObject(obj, planetObjects[i].angle, planetObjects[i].height);
            if (planetObjects[i].type == "FloatingObject")
            {
                obj.GetComponent<FloatingObject>().movingSpeed = planetObjects[i].movingSpeed;
            }
        }

        ObjectData[] buildingBlocks = data.PlayingObjects;
        if (!buildingBlock) buildingBlock = GameObject.Find("BuildingBlock");
        for (int i = 0; i < buildingBlocks.Length; i++)
        {
            string path = buildingBlocks[i].type + "/" + buildingBlocks[i].name;
            GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(Resources.Load(path));
            obj.transform.SetParent(buildingBlock.transform);
        }

        GameObject.Find("SpeedController").GetComponent<SpeedPatternGenerator>().rotationSpeedPattern = data.RotationSpeedPattern;
        FixBuildingBlockPosition();
    }
}

