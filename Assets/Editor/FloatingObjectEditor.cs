﻿using UnityEditor;
using System.Collections;

[CustomEditor(typeof(FloatingObject))]
public class FloatingObjectEditor : Editor {
    void OnSceneGUI()
    {
        if (EditorApplication.currentScene.Contains("LevelEditor"))
        {
            if (Selection.activeTransform != null && Selection.activeTransform.parent != null && Selection.activeTransform.parent.name == "Planet")
            {

                if (LevelEditor.GetPrefabPath(Selection.activeTransform.gameObject).Contains("FloatingObject"))
                {
                    LevelEditor.FixAngle(Selection.activeTransform);
                }

                if (Selection.activeTransform != null && Selection.activeTransform.parent != null && Selection.activeTransform.parent.name == "BuildingBlock")
                {
                    LevelEditor.FixBuildingBlockPosition();
                }
            }
        }
	}
}
