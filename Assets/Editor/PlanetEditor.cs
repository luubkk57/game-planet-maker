﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Building))]
public class PlanetEditor : Editor
{
    //private GameObject planet = GameObject.Find("Planet");
    void OnSceneGUI()
    {
        if (EditorApplication.currentScene.Contains("LevelEditor"))
        {
            if (Selection.activeTransform != null && Selection.activeTransform.parent != null && Selection.activeTransform.parent.name == "Planet")
            {
                if (LevelEditor.GetPrefabPath(Selection.activeTransform.gameObject).Contains("Building"))
                {
                    LevelEditor.FixAngle(Selection.activeTransform);
                    LevelEditor.FixPosition(Selection.activeTransform);
                }
            }

            if (Selection.activeTransform != null && Selection.activeTransform.parent != null && Selection.activeTransform.parent.name == "BuildingBlock")
            {
                LevelEditor.FixBuildingBlockPosition();
            }
        }
    }
}
