﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Collections;

public class GameController : MonoBehaviour
{
    public GoogleAnalyticsV3 googleAnalytics;
    public LeaderBoardController leaderboard;
    
    public static GameController instance;
#if UNITY_EDITOR
    public float timeScale = 1;
#endif
    public GameObject planet;
    public PlanetController planetController;

    public GameObject[] buildingBlocks;
    
    private int currentIndex = 0;
    //private int count = 0;
    //Vector3 nextBuildingPosition = new Vector3(-3, 5.5f, 0);
    public Vector3 defaultPosition = new Vector3(0, 5.25f, 0);
    private float nextSpeed;
    //private float radius = 0;
    private int dropSpeed = 100;
    private bool isDropping = false;
    public bool isCollision = false;

    public int score = 0;
    public Vector2 target;
    //private float MaxDistance = 1;    
    //private bool canDrop = false;
    private int rotationIndex = 0;
    private SpeedPattern[] speedPattern;
    private bool variableSpeed = false;
    //private int currentLevelIndex = 1;

    public bool isButtonPressed = false;
    
    public AdManager adManager;

    public GameObject mCamera;
    public GameObject ssCamera;
    //public GameObject ssGUI;
    public GameObject ssCameraPrefab;
    //public GameObject ssGUIPrefab;
    
    void Start()
    {
        CheckFirstTimePlay();
        instance = this;
        //currentLevelIndex = PlayerPrefs.GetInt("Current_Level");
        //Debug.Log(currentLevelIndex);
        if (!planetController) planetController = planet.GetComponent<PlanetController>();        
        //Load("Level " + MadLevel.arguments);        
        //Debug.Log(PlayerPrefs.GetInt("Current_Level"));
        //Load(currentLevelIndex);
        Load(LevelManager.currentLevel);
        target = planet.transform.position;
        target.y += planetController.GetRadius() - 0.01f;

        if (!adManager) adManager = GameObject.Find("AdManager").GetComponent<AdManager>();
        adManager.LoadInterstitial();
        //adManager.HideBanner();
        googleAnalytics.LogScreen("Play level " + PlayerPrefs.GetInt(Attributes.CURRENT_LEVEL));
        //AndroidGoogleAnalytics.instance.SendView("Play level " + PlayerPrefs.GetInt(Attributes.CURRENT_LEVEL));
        adManager.ShowBanner();
    }

    void FixedUpdate()
    {        
        if (GameState.instance.gameState == State.Playing)
        {
            if (currentIndex < buildingBlocks.Length && isDropping)
            {
                if (buildingBlocks[currentIndex] != null)
                {
                    //if (Vector2.Distance(buildingBlocks[currentIndex].transform.position, target) > 0.01 && !isCollision)
                    if (buildingBlocks[currentIndex].transform.position.y > target.y)
                    {
                        buildingBlocks[currentIndex].transform.position = Vector2.MoveTowards(buildingBlocks[currentIndex].transform.position,
                            target, Time.deltaTime * dropSpeed * GameState.instance.gameSpeed);
                        //buildingBlocks[currentIndex].transform.position = Vector2.Lerp(buildingBlocks[currentIndex].transform.position,
                        //    target, Time.deltaTime * dropSpeed * GameState.instance.gameSpeed);
                        //buildingBlocks[currentIndex].GetComponent<Rigidbody2D>().MovePosition(buildingBlocks[currentIndex].transform.position - buildingBlocks[currentIndex].transform.up * Time.deltaTime * dropSpeed);
                    }
                    else
                    {
                        isDropping = false;
                        buildingBlocks[currentIndex].GetComponent<Building>().isMoving = false;
                        buildingBlocks[currentIndex].transform.SetParent(planet.transform);
                        SFXManager.instance.PlaySFX(eSFX.Dropping);
                        currentIndex++;
                        int remain = buildingBlocks.Length - currentIndex;
                        GUIManager.instance.SetRemainObjects(remain.ToString());
                        if (currentIndex < buildingBlocks.Length)
                        {
                            buildingBlocks[currentIndex] = (GameObject)Instantiate(buildingBlocks[currentIndex],
                                defaultPosition, Quaternion.identity);
                        }
                        else
                        {
                            FinishedGame(true);
                        }
                    }
                }
            }
        }
    }

    void Update()
    {
        //Debug.Log(Time.time);
#if UNITY_EDITOR
        Time.timeScale = timeScale;
#endif
        CheckInput();
        if (variableSpeed) CheckSpeedPattern();
    }

    private void DropObject()
    {
        if (GameState.instance.gameState == State.Playing && !isDropping)
        {
            isDropping = true;
            buildingBlocks[currentIndex].transform.position = defaultPosition;
        }
        if (currentIndex < buildingBlocks.Length && buildingBlocks[currentIndex] != null)
            buildingBlocks[currentIndex].GetComponent<Building>().isMoving = true;
    }

    public void Load(int index)
    {
        //Debug.Log(LevelManager.Load(index));
        LevelLoader loader = new LevelLoader(LevelManager.Load(index));
        if (loader.data.Rotate != 0)
        {
            planetController.rotateSpeed = loader.data.Rotate;
            variableSpeed = false;
        }
        else
        {
            speedPattern = loader.data.RotationSpeedPattern;
            rotationIndex = 0;
            planetController.rotateSpeed = speedPattern[rotationIndex].speed;
            variableSpeed = true;
        }
        buildingBlocks = loader.playingObjects;
        buildingBlocks[currentIndex] = (GameObject)Instantiate(buildingBlocks[currentIndex], defaultPosition, Quaternion.identity);
        //Debug.Log(buildingBlocks[currentIndex].transform.position + "_" + defaultPosition);
        //GUIManager.instance.txtRemain.transform.position = Camera.main.WorldToScreenPoint(buildingBlocks[currentIndex].transform.position);
        int remain = buildingBlocks.Length - currentIndex;
        GUIManager.instance.SetRemainObjects(remain.ToString());
    }

    public void FinishedGame(bool complete)
    {
        googleAnalytics.LogScreen("Result Menu");
        SFXManager.instance.bgm.Stop();
        adManager.HideBanner();
        GameState.instance.gameState = State.LevelFinished;
        if (complete) FXManager.instance.CreateFireWorks(2.5f);
        StartCoroutine(FinishGameCoroutine(complete));
    }

    IEnumerator FinishGameCoroutine(bool complete)
    {
        GUIManager.instance.ShowCompleteMenu(complete);
        if (complete)
        {
            //yield return new WaitForSeconds(2.0f);
            //currentLevelIndex++;
            //PlayerPrefs.SetInt("Current_Level", currentLevelIndex);
            //if (PlayerPrefs.GetInt("Highest_Level") < currentLevelIndex)
            //{
            //    PlayerPrefs.SetInt("Highest_Level", currentLevelIndex);
            //    leaderboard.ReportCurrentLevel();
            //}
            LevelManager.SetNextLevel();
            //SFXManager.instance.PlaySFX(eSFX.Level_Completed);            
        }
        else
        {
            SFXManager.instance.PlaySFX(eSFX.Level_Fail);
        }
        //yield return new WaitForSeconds(0.5f);
        if (!AdManager.IsAdsRemove())
        {
            //if (!complete) adManager.ShowBanner();
            yield return new WaitForSeconds(0.5f);
            //if (complete)
            //{
            //    ShowFullScreenAd();
            //}
            ShowFullScreenAd();
        }
    }

    void CheckInput()
    {
        //Debug.Log("Current detected event: " + Event.current);
        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            if (EventSystem.current.currentSelectedGameObject && EventSystem.current.currentSelectedGameObject.name.Contains("btn"))
            {
                SFXManager.instance.PlaySFX(eSFX.Button);
            }
            else
            {
                DropObject();
            }
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            if (GameState.instance.gameState == State.Playing)
            {
                if (EventSystem.current.currentSelectedGameObject && EventSystem.current.currentSelectedGameObject.name.Contains("btn"))
                {
                }
                else
                {
                    DropObject();
                }
            }
        }
#endif
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameState.instance.gameState == State.Playing)
            {
                Pause();
            }
            else if (GameState.instance.gameState == State.Pause)
            {
                Resume();
            }
            else if (GameState.instance.gameState == State.LevelFinished)
            {
                ReturnToLevelSelector();
            }
        }
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.R))
        {
            LoadLevel(0);
        }
        if (Input.GetKeyDown(KeyCode.PageUp))
        {
            //LoadLevel(currentLevelIndex--);
            LoadLevel(LevelManager.currentLevel-1);
        }
        if (Input.GetKeyDown(KeyCode.PageDown))
        {
            //LoadLevel(currentLevelIndex++);
            if (LevelManager.currentLevel < LevelManager.Count - 1)
            {
                LoadLevel(LevelManager.currentLevel + 1);
            }
            else
            {
                LoadLevel(LevelManager.currentLevel);
            }
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            Test();
        }
#endif
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LoadNextLevel()
    {
        Application.LoadLevel("Play");
    }
#if UNITY_EDITOR
    public void LoadLevel(int level)
    {
        LevelManager.currentLevel = level;
        PlayerPrefs.SetInt(Attributes.HIGH_LEVEL, 0);
        PlayerPrefs.SetInt(Attributes.CURRENT_LEVEL, LevelManager.currentLevel);
        Application.LoadLevel("Play");
    }
#endif

    public void RetryLevel()
    {
        //if (!AdManager.IsAdsRemove()) adManager.HideBanner();
        Application.LoadLevel("Play");
    }

    public void ReturnToLevelSelector()
    {
        //if (!AdManager.IsAdsRemove()) adManager.HideBanner();
        Application.LoadLevel("MainMenu");
    }

    public void Pause()
    {
        SFXManager.instance.PlaySFX(eSFX.Button);
        GameState.instance.gameState = State.Pause;
        GUIManager.instance.GUIPauseMenu.SetActive(true);
        GUIManager.instance.GUIHUD.SetActive(false);
        if (!AdManager.IsAdsRemove()) adManager.ShowBanner();
    }

    public void TooglePause()
    {
        //Debug.Log("Pause");
        isButtonPressed = true;
        if (GameState.instance.gameState == State.Playing)
        {
            Pause();
        }        
    }

    public void Resume()
    {
        SFXManager.instance.PlaySFX(eSFX.Button);
        GUIManager.instance.GUIHUD.SetActive(true);
        GUIManager.instance.GUIPauseMenu.SetActive(false);
        GameState.instance.gameState = State.Playing;        
    }

    private float timeCount = 0;

    private void CheckSpeedPattern()
    {
        timeCount += Time.deltaTime;
        if (timeCount > speedPattern[rotationIndex].time)
        {
            if (rotationIndex < speedPattern.Length - 1)
            {
                rotationIndex++;
            }
            else
            {
                rotationIndex = 0;
            }
            planetController.rotateSpeed = speedPattern[rotationIndex].speed;
            timeCount = 0;
        }
    }

    private void CheckFirstTimePlay()
    {
        if (!PlayerPrefs.HasKey(Attributes.CURRENT_LEVEL))
        {
            //PlayerPrefs.SetInt("Current_Level", 1);
            //PlayerPrefs.SetInt("Highest_Level", 1);
            PlayerPrefs.SetInt("FullScreenAd", 0);
        }

        if (PlayerPrefs.GetInt(Attributes.CURRENT_LEVEL) == 0)
        {
            StartCoroutine(SetStatus(1.0f, "Tap anywhere on screen to drop objects"));
            StartCoroutine(SetStatus(5.0f, "Try to avoid collision"));
        }
    }

    private void ShowFullScreenAd()
    {
        if (!AdManager.IsAdsRemove()) adManager.ShowInterstitial();
    }

    public void SetStatus(string msg)
    {
        GUIManager.instance.ShowTextStatus(msg);
    }

    IEnumerator SetStatus(float time, string msg)
    {
        yield return new WaitForSeconds(time);
        SetStatus(msg);
    }
    
#if UNITY_EDITOR
    void Test()
    {
        //Debug.Log(ssCamera.GetComponent<Camera>().orthographicSize);
        //if (Camera.main == mCamera.camera)
        //{
        //    ssCamera.SetActive(true);
        //    mCamera.SetActive(false);
        //}
        //else
        //{
        //    ssCamera.SetActive(false);
        //    mCamera.SetActive(true);
        //}
        ShowCompletePicure();
    }
#endif

    public void ShowCompletePicure()
    {
        GUIManager.instance.DisableGUI(true);
        //SwitchToSSCamera(true);
        ssCamera = (GameObject)Instantiate(ssCameraPrefab);
        //ssGUI = (GameObject)Instantiate(ssGUIPrefab);
        //GUIManager.SetSSGUIText(ssGUI, "Well Done! You have finished level " + PlayerPrefs.GetInt(Attributes.CURRENT_LEVEL));
        //float camSize = ssCamera.GetComponent<Camera>().orthographicSize;
        for (int i = 0; i < planet.transform.childCount; i++)
        {
            if (planet.transform.GetChild(i).name.Contains("House_") || planet.transform.GetChild(i).name.Contains("Tree_"))
            {
                //Debug.Log(planet.transform.GetChild(i).name);
                GameObject house = (GameObject)Instantiate(planet.transform.GetChild(i).gameObject);
                house.transform.SetParent(ssCamera.transform.FindChild("SSBackground"));

                Vector2 localPos = new Vector2();
                localPos.x = 4.8f * (house.transform.localEulerAngles.z - 180) / 180;

                house.transform.localEulerAngles = Vector3.zero;
                house.transform.localPosition = localPos;
                house.transform.localScale = house.transform.localScale * 2;
                
                //Debug.Log(house.name);
            }
        }
        //FacebookManager.instance.ShareFB();        
    }

    //public void RemoveSS()
    //{
    //    GameObject.Destroy(ssCamera);
    //    GameObject.Destroy(ssGUI);
    //}

    public void SwitchToSSCamera(bool switchCamera)
    {
        if (switchCamera)
        {
            ssCamera.SetActive(true);
            mCamera.SetActive(false);
        }
        else
        {
            ssCamera.SetActive(false);
            mCamera.SetActive(true);
        }
    }
}
