﻿using UnityEngine;
using System.Collections;

public enum eSFX { Level_Completed, Level_Fail, Explode, Dropping, Button, Firework }

public class SFXManager : MonoBehaviour {

    public bool SoundOn = true;

    public AudioClip sfxLevelCompleted;
    public AudioClip sfxLevelFailed;
    public AudioClip sfxDropping;
    public AudioClip sfxExplode;
    public AudioClip sfxButton;
    public AudioClip sfxFirework;

    public AudioClip[] bgms;

    public AudioSource sfx;
    public AudioSource bgm;    

    public static SFXManager instance;

    void Start()
    {
        instance = this;
        CheckSoundSetting();
        if (!sfx) sfx = GetComponent<AudioSource>();
        if (!bgm) bgm = GameObject.Find("BGM").GetComponent<AudioSource>();        
        if (bgms.Length > 0) bgm.clip = bgms[PlayerPrefs.GetInt("Current_Level") % bgms.Length];
        if (!SoundOn) { bgm.Pause(); } else { bgm.Play(); }
        //Debug.Log(bgm.clip.length);
    }

    public void PlaySFX(eSFX sound)
    {
        if (SoundOn)
        {
            switch (sound)
            {
                case eSFX.Level_Completed:
                    sfx.clip = sfxLevelCompleted;
                    break;
                case eSFX.Level_Fail:
                    Debug.Log("SFX: Level Failed");
                    sfx.clip = sfxLevelFailed;
                    break;
                case eSFX.Dropping:
                    sfx.clip = sfxDropping;
                    break;
                case eSFX.Explode:
                    Debug.Log("SFX: Explosion");
                    sfx.clip = sfxExplode;
                    break;
                case eSFX.Button:
                    Debug.Log("SFX: Button");
                    sfx.clip = sfxButton;
                    break;
                case eSFX.Firework:
                    sfx.clip = sfxFirework;
                    break;
            }
            sfx.Play();
        }
    }

    public void CheckSoundSetting()
    {
        if (PlayerPrefs.GetInt("Setting_Sound") == 0)
        {
            SoundOn = true;
        }
        else
        {
            SoundOn = false;
        }
    }

    public void ToogleSound()
    {
        if (SoundOn)
        {
            SoundOn = false;
            PlayerPrefs.SetInt("Setting_Sound", 1);
            bgm.Pause();
        }
        else
        {
            SoundOn = true;
            PlayerPrefs.SetInt("Setting_Sound", 0);
            bgm.Play();
        }
        GUIManager.instance.ToogleSound(SoundOn);
    }
}
