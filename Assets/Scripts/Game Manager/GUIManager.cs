﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    public static GUIManager instance;

    public GameObject GUIPanel;

    public GameObject GUIEndGame;
    public GameObject GUIPauseMenu;
    public GameObject GUIHUD;
    public GameObject txtEndGame;
    public GameObject btnNextLevel;
    public GameObject btnReplay;
    public GameObject txtRemain;
    public GameObject txtCurrentLevel;

    public GameObject btnSound;
    public Sprite soundOff;
    public Sprite soundOn;

    public Text status;

    public Transform meteor;

    //private Text tScore;
    //private bool finished = false;
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        GUIEndGame.SetActive(false);
        GUIPauseMenu.SetActive(false);
        GUIHUD.SetActive(true);
        //tScore = GUIEndGame.GetComponent<Text>();
        txtCurrentLevel.GetComponent<Text>().text = (PlayerPrefs.GetInt("Current_Level") + 1).ToString();

        if (PlayerPrefs.GetInt("Setting_Sound") == 0)
        {
            btnSound.GetComponent<Image>().sprite = soundOn;
        }
        else
        {
            btnSound.GetComponent<Image>().sprite = soundOff;
        }
    }

    void Update()
    {
        //if (GameState.instance.gameState == State.LevelFinished)
        //{
        //    if (!finished)
        //    {
        //        GUIHUD.SetActive(false);
        //        GUIEndGame.SetActive(true);
        //        //StartCoroutine(GameEnd());
        //        finished = true;
        //    }
        //}
        //MSG();
        if (GameState.instance.gameState == State.Playing)
        {
            meteor.Rotate(Vector3.forward, Time.deltaTime * 45.0f);
        }
    }

    public void SetRemainObjects(string text)
    {
        txtRemain.GetComponent<Text>().text = text;
    }

    //IEnumerator GameEnd()
    //{
    //    for (int i = 0; i <= GameController.instance.score; i++)
    //    {
    //        score.GetComponent<Text>().text = i.ToString();
    //        yield return new WaitForSeconds(0.01f);
    //    }
    //}

    public void SetButtonNextLevel(bool active)
    {
        if (!btnNextLevel) btnNextLevel = GameObject.Find("btnNextLevel");
        btnNextLevel.GetComponent<Button>().interactable = active;
    }

    public void SetCompleteText(string txt)
    {
        txtEndGame.GetComponent<Text>().text = txt;
        if (txt.Contains("Fail"))
        {
            txtEndGame.GetComponent<Text>().color = Color.red;
        }
    }

    public void ShowCompleteMenu(bool isComplete)
    {
        string txtEnd = "Level Complete!";
        if (!isComplete)
        {
            txtEnd = "Level Fail!";
            //GUIEndGame.transform.FindChild("txtEndGame").GetComponent<Text>().color = Color.red;
        }
        GUIEndGame.transform.FindChild("txtEndGame").GetComponent<Text>().text = txtEnd;
        GameObject btnNextLevel = GUIEndGame.transform.FindChild("btnNextLevel").gameObject;
        GameObject btnRetry = GUIEndGame.transform.FindChild("btnRetry").gameObject;
        GameObject btnShare = GUIEndGame.transform.FindChild("btnShare").gameObject;
        if (isComplete)
        {
            if (LevelManager.currentLevel < LevelManager.Count - 1)
            {
                btnRetry.SetActive(false);
                btnNextLevel.SetActive(true);
                btnShare.SetActive(true);
            }
            else
            {
                btnNextLevel.SetActive(false);
                btnRetry.SetActive(true);
                GUIEndGame.transform.FindChild("txtCompleteAllLevel").gameObject.SetActive(true);
                Debug.Log(PlayerPrefs.GetInt(Attributes.CURRENT_LEVEL) + "_" + LevelManager.Count);
            }
            //int nextLevel = PlayerPrefs.GetInt("Current_Level") + 1;
            //if (Resources.Load("Level/Level " + nextLevel))
            //{
            //    btnNextLevel.GetComponent<Button>().interactable = true;
            //}
            //else
            //{
            //    btnNextLevel.GetComponent<Button>().interactable = false;
            //}            
        }
        else
        {
            btnNextLevel.SetActive(false);
            btnRetry.SetActive(true);
            btnShare.SetActive(false);
        }
        GUIHUD.SetActive(false);
        GUIEndGame.SetActive(true);
    }

    //public bool CheckPauseMenuPressed(Vector3 pos)
    //{
    //    //Button pauseMenu = GameObject.Find("btnPauseMenu").GetComponent<Button>();

    //    //return pauseMenu.guiText.HitTest(Camera.main.WorldToScreenPoint(pos));
    //    return false;
    //}

    public void ToogleSound(bool on)
    {
        if (on)
        {
            btnSound.GetComponent<Image>().sprite = soundOn;
        }
        else
        {
            btnSound.GetComponent<Image>().sprite = soundOff;
        }
    }

    public void ShowTextStatus(string msg)
    {
        status.text = msg;
        if (!status.gameObject.active) status.gameObject.SetActive(true);
        status.GetComponent<Animator>().SetTrigger("FadeIn");
    }

    public void DisableGUI(bool disable)
    {
        GUIPanel.SetActive(!disable);
    }

    //#if UNITY_EDITOR
    //    string msg = "test";
    //    public void MSG()
    //    {
    //        //GameObject pauseMenu = GameObject.Find("btnPauseMenu");
    //        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //        //RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 10);
    //        //msg = "" + hit.transform.name;
    //    }
    //    void OnGUI()
    //    {
    //        //GUI.Label(new Rect(0, 0, 500, 100), Input.mousePosition.ToString());
    //        //GUI.Label(new Rect(0, 0, 500, 100), Camera.main.WorldToViewportPoint(Input.mousePosition).ToString());
    //        GUI.Label(new Rect(0, 0, 500, 100), msg);
    //    }
    //#endif

    public static void SetSSGUIText(GameObject SSGUI, string text)
    {
        SSGUI.GetComponentInChildren<Text>().text = text;
    }
}