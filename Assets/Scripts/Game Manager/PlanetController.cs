﻿using UnityEngine;
using System.Collections;

public class PlanetController : MonoBehaviour
{
    public float rotateSpeed = -10f;
    //public float radius = 0;
    Vector2 position;

    void Start()
    {
        //SetRadius();
        position.x = transform.position.x;
        position.y = transform.position.y;
    }

    void Update()
    {
        if (GameState.instance.gameState == State.Playing)
        {
            transform.Rotate(Vector3.back * Time.deltaTime * rotateSpeed * GameState.instance.gameSpeed);
        }
    }

    public Vector2 GetPosition(float angle, float height)
    {
        float radius = GetRadius() + height;
        Vector2 pos = new Vector2();
        angle = angle * Mathf.Deg2Rad;
        pos.x = position.x + radius * Mathf.Sin(angle);
        pos.y = pos.y + radius * Mathf.Cos(angle);
        return pos;
    }
    public Vector2 GetPosition(float angle)
    {
        return GetPosition(angle, 0);
    }

    public void PutObject(GameObject go, float degree, float height)
    {
        go.transform.SetParent(transform);
        go.transform.localEulerAngles = new Vector3(0, 0, -degree);
        go.transform.localPosition = GetPosition(degree, height);
    }

    public void PutObject(GameObject go, float degree)
    {
        PutObject(go, degree, 0);
    }

    public float GetDistance(float from, float to)
    {
        Vector2 vFrom = GetPosition(from);
        Vector2 vTo = GetPosition(to);
        return Vector2.Distance(vFrom, vTo);
    }

    public float GetRadius()
    {
        return GetComponent<CircleCollider2D>().radius;
    }

    public Vector2 CreateRandomPosition(float maxHeight)
    {
        return GetPosition(Random.Range(0, 360), Random.Range(0, maxHeight));
    }
}
