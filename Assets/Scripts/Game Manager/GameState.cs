﻿using UnityEngine;
using System.Collections;

public enum State { Playing, Quit, GameOver, Title, LevelFinished, Pause, Collision }

public class GameState : MonoBehaviour {

    public static GameState instance;

    public State gameState = State.Playing;

    public float gameSpeed = 1;
    public float replaySpeed = 0.01f;

	void Start () {
        instance = this;
	}
}
