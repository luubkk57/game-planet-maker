﻿using UnityEngine;
using System.Collections;
using System.IO;

public class FXManager : MonoBehaviour {
    public class Effect
    {
        GameObject effect;
    }
    //public GameObject explosion;
    //public bool isEffect = false;

    //private GameObject explosionFx;
    //private Animator animator;
    public static FXManager instance;

    public ParticleSystem exlosion;
    public ParticleSystem firework;

    void Start()
    {
        instance = this;
    }


    public void CreateExplosion(Vector2 posision)
    {
        //isEffect = true;
        //explosionFx = (GameObject)Instantiate(explosion, posision, Quaternion.identity);
        //animator = explosionFx.GetComponent<Animator>();
        SFXManager.instance.PlaySFX(eSFX.Explode);
        Instantiate(exlosion.gameObject, posision, Quaternion.identity);
    }

    public void CreateFirework(Vector2 position)
    {
        Instantiate(firework.gameObject, position, Quaternion.identity);        
    }

    IEnumerator CreateRandomFireWorks(float time)
    {
        float playTime = 0;
        while (playTime < time)
        {
            playTime += 0.5f;
            //Debug.Log(playTime);
            CreateFirework(GameController.instance.planetController.CreateRandomPosition(3.0f));
            SFXManager.instance.PlaySFX(eSFX.Firework);
            yield return new WaitForSeconds(0.4f);
        }
    }

    public void CreateFireWorks(float time)
    {
        StartCoroutine(CreateRandomFireWorks(time));
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    CreateExplosion(new Vector2(0, 0));
        //}

        //if (explosionFx)
        //{
        //    //Debug.Log(animator.GetAnimatorTransitionInfo(0).ToString() + animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        //    if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > animator.GetCurrentAnimatorStateInfo(0).length)
        //    {
        //        //Debug.Log(animator.GetCurrentAnimatorStateInfo(0).length);
        //        Destroy(explosionFx);
        //        isEffect = false;
        //    }
        //}
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.P))
        {
            Test();
        }
#endif
    }

#if UNITY_EDITOR
    void Test()
    {
        //CreateFirework(new Vector2(0, 0));
        //StartCoroutine(CreateRandomFireWorks(2));
        CreateExplosion(GameController.instance.planetController.CreateRandomPosition(2));
    }
#endif
}
