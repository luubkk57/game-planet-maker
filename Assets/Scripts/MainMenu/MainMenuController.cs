﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using ChartboostSDK;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController instance;
    public GoogleAnalyticsV3 googleAnalytics;
    public Text txtCurrentLevel;

    public Sprite UISoundOff;
    public Sprite UISoundOn;
    public Image btnSoundOnOff;
    //public GameObject backGround;
    public GameObject mnuChooseLevel;
    public GameObject mnuReplayLevel;
    public GameObject mnuQuit;
    public Transform imgEarth;
    public Transform btnPlay;
    public Transform imgRocket;

    public Text txtSatus;
    public InputField inputLevel;

    public AudioSource bgm;
    public AudioSource sfxButton;

    public GameObject removeAds;
    public AdManager adManager;

    void Start()
    {
        googleAnalytics.LogScreen("Main Menu");
        if (!PlayerPrefs.HasKey(Attributes.SETTING_SOUND))
        {
            PlayerPrefs.SetInt(Attributes.SETTING_SOUND, 0);
        }
        CheckSettings();
        txtCurrentLevel.text = PlayerPrefs.GetInt(Attributes.CURRENT_LEVEL).ToString();
        Debug.Log(txtCurrentLevel.text);
        if (PlayerPrefs.GetInt(Attributes.SETTING_SOUND) == 0)
        {
            bgm.Play();
        }
        else
        {
            bgm.Stop();
        }
#if UNITY_ANDROID
        Adunion4Unity.Instance.preloadInterstitialAd();
        adManager.HideBanner();
#endif
    }

    void Update()
    {
        CheckInput();
        imgEarth.Rotate(Vector3.forward, Time.deltaTime * 5.0f);
        imgRocket.RotateAround(btnPlay.position, -Vector3.forward, Time.deltaTime * 15.0f);
    }

    public void Quit()
    {
        PlaySFXButton();
        Application.Quit();
    }

    public void Play()
    {
        PlaySFXButton();
        Application.LoadLevel("Play");
    }

    private void CheckInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowQuitDialog(!mnuQuit.activeSelf);
        }
    }

    private bool isQuitDialogShow = false;

    public void ShowQuitDialog(bool show)
    {
        PlaySFXButton();
        mnuQuit.SetActive(show);
    }

    public void ToogleSound()
    {
        PlaySFXButton();
        if (PlayerPrefs.GetInt(Attributes.SETTING_SOUND) == 0)
        {
            PlayerPrefs.SetInt(Attributes.SETTING_SOUND, 1);
            bgm.Stop(); 
            ShowTextStatus("Sound is off");
        }
        else
        {
            PlayerPrefs.SetInt(Attributes.SETTING_SOUND, 0);
            bgm.Play();
            ShowTextStatus("Sound is on");
        }
        CheckSettings();
    }

    private void CheckSettings()
    {
        if (PlayerPrefs.GetInt(Attributes.SETTING_SOUND) == 0)
        {
            btnSoundOnOff.sprite = UISoundOn;
        }
        else
        {
            btnSoundOnOff.sprite = UISoundOff;
        }
    }

    public void ShowTextStatus(string msg)
    {
        txtSatus.text = msg;
        if (!txtSatus.gameObject.active) txtSatus.gameObject.SetActive(true);
        txtSatus.GetComponent<Animator>().SetTrigger("FadeIn");
    }

    public void ShowChooseLevelMenu(bool show)
    {
        PlaySFXButton();
        mnuChooseLevel.SetActive(show);
    }

    public void ShowReplayLevelMenu(bool show)
    {
        PlaySFXButton();
        mnuReplayLevel.SetActive(show);
    }

    public void Play(int level)
    {

        if (level < 1)
        {
            ShowTextStatus("Level must be > 0");
        }
        else if (level > (PlayerPrefs.GetInt(Attributes.HIGH_LEVEL) + 1))
        {
            //level = PlayerPrefs.GetInt("Highest_Level");
            ShowTextStatus("Level must be < " + (PlayerPrefs.GetInt(Attributes.HIGH_LEVEL) + 1));
        }
        else
        {
            PlayerPrefs.SetInt(Attributes.CURRENT_LEVEL, level - 1);
            Application.LoadLevel("Play");
        }
    }

    public void PlayChosenLevel()
    {
        PlaySFXButton();
        int level = int.Parse(inputLevel.text);
        LevelManager.currentLevel = level;
        Play(level);
    }

    //IEnumerator AsyncLoad()
    //{
    //    yield return new WaitForFixedUpdate();
    //    AsyncOperation async = Application.LoadLevelAsync("Play");
    //    async.allowSceneActivation = false;
    //}
    private void PlaySFXButton()
    {
        Debug.Log("SFX: Button");
        if (PlayerPrefs.GetInt(Attributes.SETTING_SOUND) == 0) sfxButton.Play();
    }

    public void RemoveAds()
    {
        //PlayerPrefs.SetInt(Attributes.REMOVE_ADS, 1);
    }

    public void MoreGames()
    {
        Adunion4Unity.Instance.linkTo(Adunion4Unity.LINK_TYPE_MOREGAME);
    }
}
