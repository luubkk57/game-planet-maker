﻿using UnityEngine;
using System.Collections;
//using GoogleMobileAds.Api;
//using ChartboostSDK;
using GoogleMobileAds.Api;

public class AdManager : MonoBehaviour
{
    public BannerView banner;
    //public bool showBanner;
    //public InterstitialAd interstitial;


    void Start()
    {
        InitBanner();
        //Debug.Log("IAB REMOVE ADS" + IsAdsRemove());
        //RequestInterstitial();
        //Chartboost.cacheInterstitial(CBLocation.Default);
    }

    public void LoadInterstitial()
    {
    }

    public void ShowInterstitial()
    {
        Adunion4Unity.Instance.showInterstitialAd(Adunion4Unity.IAD_TYPE_GAMEGIFT);
    }

    public void InitBanner()
    {
        banner = new BannerView(Attributes.BANNERS_AD_UNIT_ID, AdSize.SmartBanner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        banner.LoadAd(request);
    }

    public void ShowBanner()
    {
        //showBanner = true;
        banner.Show();
    }

    public void HideBanner()
    {
        //showBanner = false;
        banner.Hide();
    }

    public static bool IsAdsRemove()
    {
        return (PlayerPrefs.GetInt(Attributes.IAB_REMOVE_ADS) == 0) ? false : true;
    }

    public static void RemoveAds()
    {
        PlayerPrefs.SetInt(Attributes.IAB_REMOVE_ADS, 1);
    }
}
