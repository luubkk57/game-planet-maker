﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
public class LevelLoader
{

    public string path = "Level";
    public string buildingPath = "Building";
    public string floatingObjectPath = "FloatingObject";
    public LevelData data;
    //public GameObject[] buildings;

    public GameObject[] playingObjects;
    public GameObject[] buildings;

    public LevelLoader(string name)
    {
        TextAsset levelFile = Resources.Load(path + "/" + name) as TextAsset;
        StringReader file = new StringReader(levelFile.text);
        XmlSerializer level = new XmlSerializer(typeof(LevelData));
        data = level.Deserialize(file) as LevelData;
        file.Close();
        LoadAllObjects();
    }

    private void LoadAllObjects()
    {
        buildings = new GameObject[data.PlanetObjects.Length];
        for (int i = 0; i < buildings.Length; i++)
        {
            //Debug.Log(data.PlanetObjects[i].movingSpeed + "_____" + data.PlanetObjects[i].name);
            buildings[i] = (GameObject)GameObject.Instantiate(Resources.Load(data.PlanetObjects[i].type 
                + "/" + data.PlanetObjects[i].name));
            if (data.PlanetObjects[i].type == "FloatingObject")
            {
                buildings[i].GetComponent<FloatingObject>().movingSpeed = data.PlanetObjects[i].movingSpeed;
                buildings[i].GetComponent<FloatingObject>().height = data.PlanetObjects[i].height;
            }

            if (data.PlanetObjects[i].type == "Animal")
            {
                buildings[i].GetComponent<Animal>().movingSpeed = data.PlanetObjects[i].movingSpeed;
                //Debug.Log(data.PlanetObjects[i].movingSpeed + "_____" + data.PlanetObjects[i].name);
                if (data.PlanetObjects[i].speedPattern != null)
                {
                    buildings[i].GetComponent<Animal>().movingPattern = data.PlanetObjects[i].speedPattern;
                }
            }
            GameController.instance.planetController.PutObject(buildings[i], data.PlanetObjects[i].angle, data.PlanetObjects[i].height);
        }

        playingObjects = new GameObject[data.PlayingObjects.Length];
        //Debug.Log(playingObjects.Length);
        for (int i = 0; i < playingObjects.Length; i++)
        {
            //Debug.Log(i);
            //string path = data.PlayingObjects[i].type
            //    + "/" + data.PlayingObjects[i].name;
            playingObjects[i] = Resources.Load(data.PlayingObjects[i].type 
                + "/" + data.PlayingObjects[i].name) as GameObject;
        }
    }
}
