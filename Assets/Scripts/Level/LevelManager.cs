﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public class LevelManager
{
    public static string path = "Level/LevelManager";
    public static List<string> levelList;
    public static int Count;
    public static int currentLevel;

    static LevelManager()
    {
        TextAsset levelFile = Resources.Load(path) as TextAsset;
        StringReader file = new StringReader(levelFile.text);
        XmlSerializer level = new XmlSerializer(typeof(List<string>));
        levelList = level.Deserialize(file) as List<string>;
        file.Close();
        Count = levelList.Count;
        currentLevel = PlayerPrefs.GetInt(Attributes.CURRENT_LEVEL);
    }

    public static string Load(int levelIndex)
    {
        if (levelIndex > Count)
        {
            return null;
        }
        else
        {
            return levelList[levelIndex];
        }
    }

    public static void SetNextLevel()
    {
        if (currentLevel < Count - 1)
        {
            currentLevel++;
            PlayerPrefs.SetInt("Current_Level", currentLevel);
            if (PlayerPrefs.GetInt("Highest_Level") < currentLevel)
            {
                PlayerPrefs.SetInt("Highest_Level", currentLevel);
                GameController.instance.leaderboard.ReportCurrentLevel();
            }     
        }
    }

#if UNITY_EDITOR
    public static void Reload()
    {
        TextAsset levelFile = Resources.Load(path) as TextAsset;
        StringReader file = new StringReader(levelFile.text);
        XmlSerializer level = new XmlSerializer(typeof(List<string>));
        levelList = level.Deserialize(file) as List<string>;
        file.Close();
        Count = levelList.Count;
    }
#endif
}
