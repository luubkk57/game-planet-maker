﻿public class LevelData
{
    public string Name;
    public int Difficult;
    public int Rotate;
    public SpeedPattern[] RotationSpeedPattern;

    public ObjectData[] PlanetObjects;
    public ObjectData[] PlayingObjects;
}