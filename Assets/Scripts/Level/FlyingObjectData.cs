﻿using UnityEngine;
using System.Collections;

public class FlyingObjectData {
    public string name;
    public float height;
    public float speed;
    public float startingAngle;
}
