﻿public class ObjectData {
    public string name;
    public string type;
    public float height;
    public float angle;
    public float movingSpeed;
    public SpeedPattern[] speedPattern;
}
