﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {
    public float speed = 4;

    void Start()
    {
        //transform.SetParent(GameController.instance.planet.transform);
        Vector3 scale = transform.localScale;
        scale.x = Mathf.Sign(speed);
        transform.localScale = scale;
    }
	void Update () {
        //GameController.instance.planetController.MovingAround(gameObject, speed);
        //transform.RotateAroundLocal(Vector3.forward, speed * Time.deltaTime);
        if (GameState.instance.gameState == State.Playing)
        {
            transform.RotateAround(GameController.instance.planet.transform.position, Vector3.forward, Time.deltaTime * speed * 5);
            Vector3 scale = transform.localScale;
            scale.x = Mathf.Sign(speed);
            transform.localScale = scale;
        }
	}

    void OnTriggerEnter2D(Collider2D other) 
    {
        //Debug.Log(other.transform.parent.name);
        if (other.tag == "Building")
        {
            //Debug.Log("Collision" + other.gameObject.name);
            GameController.instance.isCollision = true;
            Destroy(other.gameObject);
            //Destroy(gameObject);
            Destroy(this.gameObject);
        }        
    }
}
