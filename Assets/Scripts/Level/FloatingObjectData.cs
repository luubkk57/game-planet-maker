﻿public class FloatingObjectData {
    public string name;
    public float angle;
    public float height;
    public float movingSpeed;
    public int order;
}
