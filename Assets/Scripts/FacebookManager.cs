﻿using UnityEngine;
using System.Collections;

public class FacebookManager : MonoBehaviour {
    public static FacebookManager instance;

    void Start()
    {
        instance = this;
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }


    void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    void LoginCallback(FBResult result)
    {
        if (result.Error != null)
            Debug.Log("Error Response:\n" + result.Error);
        else if (!FB.IsLoggedIn)
        {
            Debug.Log("Login cancelled by Player");
        }
        else
        {
            Debug.Log("Login was successful!");
            ShareFB();
        }
    }

    private IEnumerator PostFBScreenshot()
    {
        yield return new WaitForEndOfFrame();
        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        //AndroidSocialGate.StartShareIntent("Share your planet!", "Show friends your planet!", tex, "facebook.katana");
        //SPFacebook.instance.PostImage("Share your planet! Level " + PlayerPrefs.GetInt(Attributes.CURRENT_LEVEL), tex);
        Destroy(tex);
        GameController.instance.SwitchToSSCamera(false);
        GUIManager.instance.DisableGUI(false);
    }

    public void ShareFB()
    {
        if (!FB.IsLoggedIn)
        {
            FB.Login("public_profile,email,user_friends", LoginCallback);
        }
        else
        {
            GameController.instance.ShowCompletePicure();
            StartCoroutine(TakeScreenshot());
        }
    }

    private IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();

        var width = Screen.width;
        var height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        byte[] screenshot = tex.EncodeToPNG();

        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
        wwwForm.AddField("message", "This my planet!!!");

        FB.API("me/photos", Facebook.HttpMethod.POST, SharePhotoCallback, wwwForm);

        GameController.instance.SwitchToSSCamera(false);
        GUIManager.instance.DisableGUI(false);
    }

    void SharePhotoCallback(FBResult result)
    {
        Debug.Log(result.Text);
    }
}
