﻿using UnityEngine;
using System.Collections;

public class Animal : MonoBehaviour {

    public float movingSpeed;
    public SpeedPattern[] movingPattern;

    void FixedUpdate()
    {
        if (GameState.instance.gameState == State.Playing)
        {
            //GameController.instance.planetController.MovingAround(gameObject, movingSpeed);
            transform.RotateAround(GameController.instance.planet.transform.position, Vector3.forward, Time.deltaTime * (movingSpeed));
            Vector3 scale = transform.localScale;
            scale.x = -Mathf.Sign(movingSpeed);
            transform.localScale = scale;
            if (movingPattern != null && movingPattern.Length > 0)
            {
                CheckSpeedPattern();
            }
        }
    }

    private float timeCount = 0;
    private int rotationIndex = 0;
    private void CheckSpeedPattern()
    {
        timeCount += Time.deltaTime;
        if (timeCount > movingPattern[rotationIndex].time)
        {
            if (rotationIndex < movingPattern.Length - 1)
            {
                rotationIndex++;
            }
            else
            {
                rotationIndex = 0;
            }
            movingSpeed = movingPattern[rotationIndex].speed;
            timeCount = 0;
        }
    }
}
