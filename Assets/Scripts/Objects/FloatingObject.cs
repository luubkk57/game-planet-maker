﻿using UnityEngine;
using System.Collections.Generic;

public class FloatingObject : MonoBehaviour {
    public float movingSpeed;
    public float height;
    
    void Update()
    {
        if (GameState.instance.gameState == State.Playing)
        {
            //GameController.instance.planetController.MovingAround(gameObject, movingSpeed);
            transform.RotateAround(GameController.instance.planet.transform.position, Vector3.forward, Time.deltaTime * (movingSpeed + GameController.instance.planetController.rotateSpeed));
            Vector3 scale = transform.localScale;
            scale.x = Mathf.Sign(movingSpeed);
            transform.localScale = scale;
        }
    }
}
