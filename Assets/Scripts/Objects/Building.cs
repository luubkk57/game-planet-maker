﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Building : MonoBehaviour
{
    public bool isMoving = false;
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (isMoving && other.gameObject.tag == "BuildingBlock")
        {
            //Debug.Log(transform.position);
            GameController.instance.isCollision = true;
            //SFXManager.instance.PlaySFX(eSFX.Explode);
            isMoving = false;
            GameState.instance.gameState = State.Collision;
            Vector2 pos = new Vector2();
            pos = (transform.position + other.transform.position) / 2;
            //Debug.Log(other.transform.tag + "__" + other.transform.name);
            col = other.gameObject;
            StartCoroutine(CreateFX(pos));
            //other.gameObject.transform.position = new Vector2(-1000, -1000);
            //transform.position = new Vector2(-1000, -1000);
        }
    }

    GameObject col;
    IEnumerator CreateFX(Vector2 position)
    {
        //FXManager.instance.CreateExplosion(new Vector2());
        yield return new WaitForSeconds(0.5f);
        col.transform.position = new Vector2(-1000, -1000);
        transform.position = new Vector2(-1000, -1000);
        //FXManager.instance.CreateExplosion(new Vector2());
        FXManager.instance.CreateExplosion(position);
        yield return new WaitForSeconds(0.5f);
        GameController.instance.FinishedGame(false);
        
        //Destroy(gameObject);
    }
}
