﻿using UnityEngine;
using System.Collections;

public class Attributes {

    public const string HIGH_LEVEL = "Highest_Level";
    public const string CURRENT_LEVEL = "Current_Level";
    public const string SETTING_SOUND = "Setting_Sound";
    
    public const string TEXT_GPG_CONNECT_OK = "Account ACCOUNT_NAME is connected!";
    public const string TEXT_GPG_CONNECT_FAIL = "Cannot Connect!";
    public const string TEXT_LAST_LEVEL = "This is the last level which you can play. Please wait for next update for new levels!";

    public const string NAME_LEADERBOARD_TOP_RANK = "leaderboard_top_rank";
    public const string ID_LEADERBOARD_TOP_RANK = "CgkI2Zanw_EBEAIQAQ";

    //public const string BANNERS_AD_UNIT_ID = "ca-app-pub-4128501448856864/9354366034";
    public const string BANNERS_AD_UNIT_ID = "ca-app-pub-4128501448856864/9354366034";
    public const string INTERSTISIALS_AD_UNIT_ID = "ca-app-pub-4128501448856864/1831099233";

    public const string IAB_UNLOCK_ALL_LEVEL = "Unlock_All_Levels";
    public const string IAB_REMOVE_ADS = "Remove_Ads";

    public const string INTERSTISIALS_ADS_TYPE = "Interstisials_Ads_Type";
}
